#!/usr/bin/perl

use strict;
use warnings;
use Config::Model 2.085 qw/cme/;
use 5.10.0;

my $control = 'debian/control';

my $status = `git status --porcelain $control`;

die "$control is changed. Please commit or reset this file\n" if $status;

my $moar_str = `moar --version`;
chomp $moar_str;
my ($moar_v) = ($moar_str =~ /MoarVM version ([\d.]+)/);

die "Can't extract versions from '$moar_str'\n"
    unless defined $moar_v;

cme('dpkg-control')->modify(
    qq!source Build-Depends:=~"s/moarvm.*/moarvm-dev (>= $moar_v)/" !,
);

system(qw/git commit/, "-m" => "control: update moarvm-dev dep versions", $control);
